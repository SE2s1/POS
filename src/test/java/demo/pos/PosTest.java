package demo.pos;

import demo.pos.presentation.Register;
import demo.pos.business.ProductService;
import demo.pos.domain.common.Money;
import demo.pos.domain.product.ProductDescription;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by overvelj on 14/11/2016.
 */
class PosTest {
	private Register r;
	private static Pos app;

	// system configuration. Only called once.
	@BeforeAll
	public static void configure() {
		app = new Pos();
	}


	//   De @BeforeEach methode initialises test data.
	// Never do this in domain classes

	@BeforeEach
	void setUp() {
		app.init();
		r = app.getRegister();

		ProductService catalog = r.getProductService();
		ProductDescription pd1 = new ProductDescription(1, "One", new Money(1.2));
		ProductDescription pd2 = new ProductDescription(2, "Two", new Money(3.54));
		catalog.addProduct(pd1);
		catalog.addProduct(pd2);

	}

	@AfterEach
	// drop DB
	public void closeDB() {
		app.close();
	}

	// All test methods are annotated with @Test
	@Test
	void createNewSaleTest() throws Exception {
		r.makeNewSale();
		// Use Assert in test methods to verify results:
		//   - postconditions of the operation contract
		//   -  return value
		//   - Correct state of participating objects
		//   - ...
		assertNotNull(r.getSale());
		assertNotNull(r.getSale().getSalesLineItems());
		assertFalse(r.getSale().isComplete());
	}

	@Test
	void addItemTest() throws Exception {
		r.makeNewSale(); // step 1
		r.enterItem(1, 10); // step 2
		assertEquals(
			1,
			r.getSale().getSalesLineItems().get(0).getPd().getId());
	}

	@Test
	void endSaleTest() throws Exception {
		r.makeNewSale();
		r.enterItem(1, 10);
		r.endSale();
		assertTrue(r.getSale().isComplete());
	}

	@Test
	void getTotalTest() throws Exception {
		r.makeNewSale();
		r.enterItem(1, 10);
		r.enterItem(2, 5);
		r.endSale();
		assertEquals(new Money((10 * 1.2) + (5 * 3.54)), r.getTotal());
	}

	@Test
	void makePaymentTest() throws Exception {
		r.makeNewSale();
		r.enterItem(1, 10);
		r.enterItem(2, 5);
		r.endSale();
		assertEquals(0, r.getStore().countSales());
		Money payment = new Money(30);
		r.makePayment(payment);
		assertEquals(payment, r.getSale().getPayment().getSum());
		assertEquals(1, r.getStore().countSales());
	}


}
