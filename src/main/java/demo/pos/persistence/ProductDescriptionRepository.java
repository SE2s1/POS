package demo.pos.persistence;

import demo.persistence.Repository;
import demo.pos.domain.common.Money;
import demo.pos.domain.product.ProductDescription;

import java.util.Collection;

/**
 * @author Jan de Rijke.
 */

public interface ProductDescriptionRepository extends Repository<Long,ProductDescription> {
	Collection<ProductDescription> findWithMaxPrice(Money budget);
}
