plugins {
    java
    application
}

group = "be.kdg.se2.POS"
version = "1.0"

repositories {
    mavenCentral()
}

application {
    mainClass.set("demo.pos.application.App")
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.11.3"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.0")
    implementation("org.apache.derby:derby:10.15.2.0")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
        showStandardStreams = true
    }
}