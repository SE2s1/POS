package demo.pos.domain.product;

import demo.pos.domain.common.Money;

/**
 * Created by overvelj on 14/11/2016.
 */
public class ProductDescription {
    private long itemId;
    private String desc;
    private Money price;

    public ProductDescription(long productId, String desc, Money price) {
        this.itemId = productId;
        this.desc = desc;
        this.price = price;
    }
    public String getDescription() {
        return desc;
    }

    public void setDescription(String desc) {
        this.desc = desc;
    }

    public Money getPrice() {
        return price;
    }

    public void setPrice(Money price) {
        this.price = price;
    }

	public void setId(long id) {
    	itemId=id;
	}

    public Long getId() {
        return itemId;
    }
}
