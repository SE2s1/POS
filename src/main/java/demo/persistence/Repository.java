package demo.persistence;

/**
 * Created by jan on 3/12/2016.
 *
 */
public interface Repository<K,V> {

	boolean update(K key,V value);

	V insert(K key, V value);

	V findById(K id);

	long count();

}
