package demo.pos.domain.sale;

import demo.pos.domain.common.Money;
import demo.pos.domain.product.ProductDescription;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by overvelj on 14/11/2016.
 */
public class Sale {
    private long id;
		private List<SalesLineItem> slis = new ArrayList<>();
    private boolean isComplete;
    private Payment payment;
    // id is unsafe when reading another series of ids from db, should be UUID
    private AtomicLong nextId = new AtomicLong(1);
    private AtomicLong nextLineItemId = new AtomicLong(1);

    public Sale() {
        id=nextLineItemId.getAndIncrement();
    }

    public Sale(long id, boolean isComplete,  Payment payment) {
        this.id = id;
        this.isComplete = isComplete;
        this.payment = payment;
    }

    public Sale(long id, boolean is_complete) {
        this(id,is_complete,null);
    }


    // aggregate members are immutable
    public List<SalesLineItem> getSalesLineItems(){
        return Collections.unmodifiableList(slis);
    }

    synchronized
    public void makeSalesLineItem(ProductDescription itemDesc, int qty) {
        slis.add(new SalesLineItem(nextLineItemId.getAndIncrement(),itemDesc,qty));
    }

    public Payment getPayment() {
        return payment;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(){
        isComplete = true;
    }

    public Money getTotal() {
        Money total=new Money(0);
        for(SalesLineItem sli : slis){
            total = total.calculate((t,s) -> t+s,sli.getSubTotal());
        }
        return total;
    }

    public void makePayment(Money i) {
        payment = new Payment(i);
    }



	public Long getId() {
    	return id;
	}

    public void setId(Long key) {
        id=key;
    }
}
