package demo.pos.domain.sale;

import demo.pos.domain.common.Money;

/**
 * Created by overvelj on 14/11/2016.
 */
public class Payment {

    private Money sum;


	public Money getSum() {
		return sum;
	}

	public Payment(Money amount) {
        this.sum = amount;
    }
}
