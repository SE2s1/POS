#POS  process sale

This is an implementaton of the **process sale** use case in the POS business case in
Applying UML and patterns (Larman), based on a 3-layer architecture.

## important branches

### master

Base case using
- JUnit 5 tests
- Two possible persistence mechanisms
  - memory (hashmaps) with GenericMemory implementation
  - Apache derby in memory database
- application.properties is used to choose a persistence mechanism
-  stateful controller
- gradle, no spring

### SpringCucumberFromGradle
branch of dependencyInverted
- based on Spring with cucumbertests that can be started from Gradle

##  informational: branches showing an implementation variant

### statelessController
branch of DependencyInverted
- stateless controller
  - alle external communication based on id's
- does not use gradle
  warning: switch from a gradle branch to this branch leaves gradle files in your  working dir!

- JUnit 5 tests
- Two possible persistence mechanisms
  - memory (hashmaps) with GenericMemory implementation
  - Apache derby in memory database
- Business classes use memory repository

### statelessControllerRepoFactories
- branch of statelessController, factory for repositories added
- - does not use gradle
  warning: switch from a gradle branch to this branch leaves gradle files in your  working dir!



## Older less interesting branches

### DependencyInverted
older branch of initial
- does not use gradle
  warning: switch from a gradle branch to this branch leaves gradle files in your  working dir!

- JUnit 5 tests
- Two possible persistence mechanisms
  - memory (hashmaps) with GenericMemory implementation
  - Apache derby in memory database
- Business classes use memory repository
- 
### SpringDI
branch of dependencyInverted
- based on Spring and Gradle

### SpringDB
branch of springDI using DB
work in progress

### SpringCucumber
branch of dependencyInverted
- based on Spring with cucumbertests