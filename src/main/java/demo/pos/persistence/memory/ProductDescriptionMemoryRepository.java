package demo.pos.persistence.memory;

import demo.persistence.memory.MemoryRepository;
import demo.pos.domain.common.Money;
import demo.pos.domain.product.ProductDescription;
import demo.pos.persistence.ProductDescriptionRepository;

import java.util.Collection;

/**
 * Created by overvelj on 14/11/2016.
 */
public class ProductDescriptionMemoryRepository extends MemoryRepository<Long,ProductDescription> implements ProductDescriptionRepository {


    @Override
    public Collection<ProductDescription> findWithMaxPrice(Money budget) {
        //budget slightly increased because of inaccuracy of double
        return findBy(p -> p.getPrice().lowerThenOrEqual (budget));
    }


}
