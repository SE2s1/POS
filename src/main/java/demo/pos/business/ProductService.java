package demo.pos.business;

import demo.pos.domain.product.ProductDescription;
import demo.pos.persistence.ProductDescriptionRepository;

/**
 * Created by overvelj on 14/11/2016.
 */
public class ProductService {
    private final ProductDescriptionRepository products  ;

    public ProductService(ProductDescriptionRepository productDescriptionRepository) {
        products = productDescriptionRepository;
    }


    public void addProduct(ProductDescription pd) {
        products.insert(pd.getId(), pd);
    }

    public ProductDescription getProductDesc(long id) {
        return products.findById(id);
    }


}
