package demo.pos;

import demo.persistence.Repository;
import demo.persistence.db.DbConnection;
import demo.persistence.memory.MemoryRepository;
import demo.pos.domain.sale.Sale;
import demo.pos.presentation.Register;
import demo.pos.business.ProductService;
import demo.pos.business.SaleService;
import demo.pos.persistence.ProductDescriptionRepository;
import demo.pos.persistence.db.ProductDescriptionDbRepository;
import demo.pos.persistence.db.SaleDbRepository;
import demo.pos.persistence.memory.ProductDescriptionMemoryRepository;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author Jan de Rijke.
 */
public class Pos {


	private static final Logger logger = Logger.getLogger("demo.pos.Pos");
	private SaleService storeSvc;
	private ProductService catalogSvc;
	private Register register;
	private Repository<Long,Sale> saleRepository;
	private ProductDescriptionRepository productDescriptionRepository;
	private Properties config=new Properties();
	private String dbUrl;
	private PersistenceType persistence;


	public Pos() {
		try (InputStream is = getClass()
			.getResourceAsStream("/application.properties")) {
			if(is != null) {
				config.load(is);
				logger.info("application properties loaded");
			}else{
				logger.info("application properties file not found in class path root, using defaults");
			}
		} catch (IOException e) {
			logger.info("error reading application properties , using defaults");
		}
			persistence = "jdbc".equals(config.getProperty("persistence", "memory"))?PersistenceType.DB:PersistenceType.MEMORY;

	}

	public void init() {
		initRepos();
		storeSvc = new SaleService(saleRepository);
		catalogSvc = new ProductService(productDescriptionRepository);
		register = new Register(catalogSvc, storeSvc);

	}

	private void initRepos() {
		if (persistence == PersistenceType.DB) {
			dbUrl = config.getProperty("persistence.url", "jdbc:derby:memory:;create=true");
			DbConnection.init(dbUrl);
			productDescriptionRepository = new ProductDescriptionDbRepository();
			saleRepository = new SaleDbRepository(productDescriptionRepository);
		} else{
			saleRepository = new MemoryRepository<>();
			productDescriptionRepository = new ProductDescriptionMemoryRepository();
		}
	}

	public void close() {
		if (persistence == PersistenceType.DB) {
			DbConnection.close();
			if ("test".equals(config.getProperty("profile", "test"))) {
				// drop the DB
				// will only have an effect for a non memory db
				String closeUrl = config.getProperty("persistence.url.drop");
				if (closeUrl == null || closeUrl.isEmpty()) {
					closeUrl = dbUrl.replace("create", "drop");
				}
				DbConnection.init(closeUrl);
				DbConnection.getInstance();
				DbConnection.close();
			}
		}
	}


	public Register getRegister() {
		return register;
	}

	public void reset() {
	}

	private enum PersistenceType {
		DB,
		MEMORY
	}
}
